# Project 4: Brevet time calculator with Ajax 
By: Sophie Ahlberg
Contact: sahlberg@uoregon.edu

This project reimplements the RUSA ACP controle time calculator, featuring flask,  ajax and testing.

Credits to Michal Young for the initial version of this code.

This project is meant  to calculate the open times and the closed times in a similar style to that of the RUSA ACP controle time calculator. As of now, the calculator receives information from two tables, one for closed time and one for open time, located in the python document acp_times.py. The calculator takes the input of kilometers which is given by the user, turns it into miles, and uses the two tables to determine what the open and closed time will be.

However, as of now, the code is not finished. Two nosetests cases have been given, calculating big distances and small distances, and they both come out with a fail. The formula of calculating the time of distance is therefore incorrect and needs to be re-evaluated. 
