import acp_times
import nose
import arrow

def test_min():
	'''
	Tests a control value  less than or equal to 200, which is the smallest integers on the table.
	'''
	time = arrow.utcnow()
	
	assert acp_times.open_time(20, 200, arrow.get(time)) == (time.shift(hours=+0,minutes=+38)).isoformat()
	assert acp_times.close_time(20, 200, arrow.get(time)) == (time.shift(hours=+2, minutes =+45)).isoformat()

#def test_max():
	'''
	Tests one of the largest control values
	'''
	#time = arrow.utcnow()
	
	#assert acp_times.open_time(900, 1000, arrow.get(time)) == (time.shift(hours=+8,minutes=+6)).isoformat()
	#assert acp_times.close_time(900, 1000, arrow.get(time)) == (time.shift(hours=+7,minutes=+18)).isoformat()